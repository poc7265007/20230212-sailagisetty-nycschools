//
//  APIService.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/13/23.
//

import Foundation

protocol HTTPClient {
    func sendRequest<T: Decodable>(endpoint: URL, responseModel: T.Type) async throws -> T
}

/// Usage let (data, response) = try await URLSession.shared.data(for: request, delegate: nil)
extension HTTPClient {
    func sendRequest<T: Decodable>(
        endpoint: URL,
        responseModel: T.Type
    ) async throws -> T {

        let urlRequest = URLRequest(url: endpoint)
        do {
            let (data, response) = try await URLSession.shared.data(for: urlRequest, delegate: nil)
            guard let response = response as? HTTPURLResponse else {
                throw APIRequestError.noResponse
            }
            switch response.statusCode {
            case 200...299:
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                guard let decodedResponse = try? decoder.decode(responseModel, from: data) else {
                    throw APIRequestError.decode
                }
                return decodedResponse
            case 401:
                throw APIRequestError.unauthorised
            default:
                throw APIRequestError.unknown
            }
        } catch URLError.Code.notConnectedToInternet {
            throw APIRequestError.offline
        } catch {
            throw APIRequestError.unknown
        }
    }
}
