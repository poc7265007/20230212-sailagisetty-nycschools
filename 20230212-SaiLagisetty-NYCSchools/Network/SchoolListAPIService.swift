//
//  SchoolListAPIService.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/13/23.
//

import Foundation

protocol SchoolAPIServiceProtocol {
    func getSchools() async throws -> [School]
    func getSchoolsSAT() async throws -> [SATDetails]
}

struct SchoolApiService: HTTPClient, SchoolAPIServiceProtocol {
    func getSchools() async throws -> [School] {
        return try await sendRequest(endpoint:
                                        URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")!,
                                     responseModel: [School].self)
    }
    
    func getSchoolsSAT() async throws -> [SATDetails] {
        return try await sendRequest(endpoint:
                                        URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")!,
                                     responseModel: [SATDetails].self)
    }
}
