//
//  SchoolListView.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/12/23.
//

import SwiftUI

struct SchoolListView: View {
    @StateObject var viewmodel = SchoolListViewModel()

    @State var query: String = ""
    var body: some View {
        VStack {
            if viewmodel.schools.count != 0 {
                VStack(alignment: .leading) {
                    List {
                        ForEach(viewmodel.schools, id: \.self) { school in
                            VStack(alignment: .leading, spacing: 12) {
                                Text(school.schoolName ?? "")
                                    .font(.title)
                                    .fontWeight(.bold)
                                Text(school.overviewParagraph ?? "")
                                    .font(.body)
                                    .fontWeight(.medium)
                            }.onTapGesture {
                                viewmodel.selectedSchool(dbn: school.dbn ?? "")
                            }
                        }
                    }
                }
                .sheet(item: $viewmodel.selectedSchoolSatScore) { selectedPost in
                    Text(selectedPost.satWritingAvgScore ?? "")
                }
            }
        }.withErrorHandling(error: $viewmodel.viewModelError)
        .onAppear {
            Task {
                await viewmodel.getSchools()
            }
        }
    }

}
