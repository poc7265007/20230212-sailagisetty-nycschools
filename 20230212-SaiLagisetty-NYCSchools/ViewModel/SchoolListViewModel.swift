//
//  SchoolListViewModel.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/13/23.
//

import Foundation
import SwiftUI

@MainActor
class SchoolListViewModel: ObservableObject {
    
    @Published var schools: [School] = []
    @Published var satScores: [SATDetails] = []
    @Published var viewModelError: SchoolViewModelError?
    @Published var isLoading: Bool = true
    @Published var selectedSchoolSatScore: SATDetails?
    @Published var noSatScoreAvailable = false
    
    var apiService = SchoolApiService()
    
    func getSchools() async {
        self.viewModelError = nil
        do {
            isLoading = true
            
            schools = try await apiService.getSchools()
            satScores = try await apiService.getSchoolsSAT()
            
            isLoading = false
        } catch APIRequestError.offline {
            self.viewModelError = .notConnectedToInternet
            self.isLoading = false
        } catch {
            self.viewModelError = .couldNotDeleteHeroe
            self.isLoading = false
        }
    }
    
    func selectedSchool(dbn: String) {
        guard let score = self.satScores.first(where: { $0.dbn == dbn }) else {
            self.viewModelError = .satScoreNotAvailable
            return
        }
        self.selectedSchoolSatScore = score
    }
}
