//
//  ErrorHandling.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/13/23.
//

import Foundation
import SwiftUI

protocol GenericErrorEnum: Error, Identifiable {
    var title: String { get }
    var errorDescription: String { get }
}

struct HandleErrorsByShowingAlertViewModifier<T>: ViewModifier where T: GenericErrorEnum {
    @Binding var error: T?

    func body(content: Content) -> some View {
        content
            .background(
                EmptyView()
                    .alert(item: $error) { viewModelError in
                        return Alert(title: Text(viewModelError.title),
                                     message: Text(viewModelError.errorDescription),
                                     dismissButton: .default(Text("OK")))

                    }
            )
    }
}

extension View {
    func withErrorHandling<T: GenericErrorEnum>(error: Binding<T?>) -> some View {
        modifier(HandleErrorsByShowingAlertViewModifier(error: error))
    }
}

enum SchoolViewModelError: GenericErrorEnum {
    // In order to make it Identifiable
    var id: Self { self }

    case couldNotDeleteHeroe
    case couldNotUpdateHeroe
    case notConnectedToInternet
    case satScoreNotAvailable
    case unknown
    case none

    var title: String {
        switch self {
        case .notConnectedToInternet:
            return "No Internet Connection"
        default:
            return "Error"
        }
    }

    var errorDescription: String {
        switch self {
        case .couldNotUpdateHeroe, .couldNotDeleteHeroe:
            return "Heroes Could not be updated"
        case .notConnectedToInternet:
            return "Please enable Wifi or Cellular data"
        case .unknown:
            return "An Unknown Error has occurred"
        case .satScoreNotAvailable:
            return "SAT score unavailable"
        case .none:
            return ""
        }
    }
}
