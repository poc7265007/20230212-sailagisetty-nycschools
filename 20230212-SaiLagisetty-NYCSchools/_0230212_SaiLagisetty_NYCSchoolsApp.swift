//
//  _0230212_SaiLagisetty_NYCSchoolsApp.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/12/23.
//

import SwiftUI

@main
struct _0230212_SaiLagisetty_NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            SchoolListView()
        }
    }
}
