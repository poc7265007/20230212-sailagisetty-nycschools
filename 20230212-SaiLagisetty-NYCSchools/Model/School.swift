//
//  School.swift
//  20230212-SaiLagisetty-NYCSchools
//
//  Created by Sai Lagisetty on 2/12/23.
//

import Foundation

struct School: Decodable, Hashable, Identifiable {
    var id = UUID()
    let dbn: String?
    let schoolName: String?
    let boro: String?
    let overviewParagraph: String?
    let location: String?
    let phoneNumber: String?
    let faxNumber: String?
    let website: String?
    let totalStudents: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    
    private enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName
        case boro
        case overviewParagraph
        case location
        case phoneNumber
        case faxNumber
        case website
        case totalStudents
        case city
        case zip
        case stateCode

    }
}
